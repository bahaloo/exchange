package com.farazpardazan.Exchange.controller;
import com.farazpardazan.Exchange.enums.CCY;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Random;

@RestController
public class ExchangeController {

    @RequestMapping(method = RequestMethod.GET, value = "/exchange/{srcCCY}/{desCCY}/{amount}")
    public ResponseEntity<Integer> exchange(@PathVariable("srcCCY") CCY srcCCY,
                            @PathVariable("desCCY") CCY desCCY,
                            @PathVariable("amount") Integer amount) {
        Random rand = new Random();

        System.out.println("rand : " + rand);

        if (srcCCY == CCY.dollar && desCCY == CCY.rial) {
            float randValue = rand.nextInt(100)/100;
            Integer res =  Math.round(110000 * amount + 50 * randValue);
            return new ResponseEntity<Integer>(res, HttpStatus.OK);
        } else {
            return new ResponseEntity<Integer>(amount, HttpStatus.OK);
        }
    }

}